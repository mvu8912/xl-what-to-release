#!/usr/bin/env perl
use strict;
use warnings;
use XL::WhatToRelease;

my @commits = XL::WhatToRelease->diff_master_and_production;

my @merges = XL::WhatToRelease->commints_info(@commits);

XL::WhatToRelease->report( @merges );
