package XL::WhatToRelease;

use strict;
use warnings;
use IO::File;
use JSON::XS qw( decode_json );
use LWP::UserAgent;

=head1 NAME

XL::WhatToRelease - Generate a copy and paste text for email what to release to production

=head1 USAGE

Update your environment variables

 export PERL_CARTON_MIRROR=http://username:password@pinto.resume-library.com

 export PROJECT_REPO_NAME=resume-library
 export PRODUCTION_SHA_URL=http://www.resume-library.com/GIT_REV.txt

Add This module to your cpanfile

 requires "XL::WhatToRelease";

Call the script

 what_to_release.pl 

=cut

my $REPO_NAME = $ENV{PROJECT_REPO_NAME}
    or die "Missing project repo name";

my $PRODUCTION_SHA_URL = $ENV{PRODUCTION_SHA_URL}
    or die "Missing the url to get the production sha";

my $OUT_FILE = $ENV{OUT_FILE};

my $OUTPUT = $OUT_FILE ? IO::File->new( ">$OUT_FILE" ) : \*STDOUT;

my $PREVIOUS_SHA = $ENV{PREVIOUS_SHA};

sub diff_master_and_production {
    my $class          = shift;
    my @merges         = _merges_of_master();
    my $production_sha = _production_sha();
    my @diff           = ();

    foreach my $merge (@merges) {
        last
            if $merge =~ /^$production_sha/;
        push @diff, $merge;
    }
    return @diff;
}

sub _merges_of_master {
    system "echo 'Cwd: ' . `pwd`";
    system "git add --all";
    system "git stash";
    system "git checkout master";
    system "git pull";
    my @merges = split /\n/, qx{git log --oneline | grep Merged};
    return grep { chomp; m/Merged in .+ \(pull request #\d+\)$/ || m/Merged .+ into master$/ } @merges;
}

sub _production_sha {
    my $sha = $PREVIOUS_SHA || do {
        my $ua = LWP::UserAgent->new;
        my $re = $ua->get($PRODUCTION_SHA_URL);
        $re->content;
    };
    return substr $sha, 0, 7;
}

sub commints_info {
    my $class   = shift;
    my @commits = @_;
    my @merges  = ();

    foreach my $commit (@commits) {
        my ( $sha, $branch, $pr_id )
            = ( $commit
                =~ /^([a-f0-9]{7}) Merged in (\S+) \(pull request #(\d+)\)/ );

        if ( !$pr_id ) {
            ( $sha, $branch ) = ( $commit =~ /^([a-f0-9]{7}) Merged (\S+) into master/ );
        }

        next
            if !$sha;

        my %info
            = ( $pr_id
            ? $class->pull_request($pr_id)
            : $class->direct_commit($sha) )
            or next;

        push @merges,
            {
            %info,
            branch => $branch,
            pr_id  => ( $pr_id || 0 ),
            sha    => $sha,
            };
    }
    return @merges;
}

sub pull_request {
    my $class = shift;

    my $id = shift
        or return;

    my $base_url = q{https://rl-merges:vAtj4Sz2DOL5@bitbucket.org};

    my $path
        = "/api/2.0/repositories/resumelibrary/$REPO_NAME/pullrequests/$id";

    my $resp = LWP::UserAgent->new->get("$base_url/$path");

    return
        if !$resp->is_success;

    my $info = eval { decode_json $resp->content }
        or return;

    return
        if $info->{destination}{branch}{name} ne "master";

    return (
        title  => $info->{title},
        author => $info->{author}{display_name},
    );
}

sub direct_commit {
    my $class = shift;

    my $sha = shift
        or return;

    my $base_url = q{https://rl-merges:vAtj4Sz2DOL5@bitbucket.org};

    my $path
        = "/api/2.0/repositories/resumelibrary/$REPO_NAME/commits/$sha";

    my $resp = LWP::UserAgent->new->get("$base_url/$path");

    return
        if !$resp->is_success;

    my $info = eval { decode_json $resp->content }
        or return;

    my $commit = $info->{values}[0];

    return (
        title  => $commit->{message},
        author => $commit->{author}{raw},
        date   => $commit->{date},
    );
}

sub report {
    my $class  = shift;
    my @merges = @_;

    if ($OUT_FILE) {
        print "Output to $OUT_FILE\n\n";
    }

    print "Email BEGIN ...\n";

    @merges
        = sort { $a->{author} cmp $b->{author} || $a->{pr_id} <=> $b->{pr_id} }
        @merges;

    foreach my $merge (@merges) {
        print $OUTPUT
            "* $merge->{author} please review `$merge->{title}` ($merge->{branch} - $merge->{sha})\n";

        print $OUTPUT
            $merge->{pr_id} ? "- https://bitbucket.org/resumelibrary/$REPO_NAME/pull-request/$merge->{pr_id}\n\n" : "- Direct Merged to Master at $merge->{date}\n\n";
    }

    print "Email END ...\n";
}

1;
